const gulp = require('gulp');
const htmlValidator = require('gulp-w3c-html-validator');
const bemValidator = require('gulp-html-bem-validator');
const include = require('gulp-include')

module.exports = function html() {
    return gulp.src('src/**/*.html')
        .pipe(include())
        /*.pipe(htmlValidator())*/
        /*.pipe(bemValidator())*/
        .pipe(gulp.dest('build'))
}