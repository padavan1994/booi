const gulp = require('gulp');
const terser = require('gulp-terser');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const include = require('gulp-include')

module.exports = function script() {
    return gulp.src('src/js/*.js')
        .pipe(include())
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(sourcemaps.write())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('build/js'))
        .pipe(gulp.dest('wp-content/themes/booi/js'))
}
