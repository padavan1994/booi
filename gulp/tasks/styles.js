const gulp = require('gulp');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const shorthand = require('gulp-shorthand');
const autoprefixer = require('gulp-autoprefixer');
const gulpStylelint = require('gulp-stylelint');
const replace = require('gulp-replace');

module.exports = function styles() {
    return gulp.src('src/styles/*.scss')
        .pipe(plumber())
        /*.pipe(gulpStylelint({
            failAfterError: false,
            reporters: [
                {
                    formatter: 'string',
                    console: true
                }
            ]
        }))*/
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass())
        .pipe(plumber())
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(shorthand())
        .pipe(cleanCSS({
            debug: true,
            compatibility: '*'
        }, details => {
            console.log(`${details.name}: Original size:${details.stats.originalSize} - Minified size: ${details.stats.minifiedSize}`)
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('wp-content/themes/booi'))
        .pipe(replace('img/', '/img/'))
        .pipe(gulp.dest('build/css'))
}
