
//=require ./swiper.min.js
//=require ./vue.min.js
//=require ./jcf.js
//=require ./jcf.scrollable.js
//=require ./jcf.select.js

(function ($) {
    $(document).ready(() => {
        window.app = new Vue({
            el: '#games',
            data: {
                perPage: 12,
                action : 'get_post_by_tag',
                showMore: true,
                loaded: false,
                allProviders: true,
                category: null,
                games: [],
                localFilter: {
                    search: ''
                },
                filterData: {
                    tag: 0,
                }
            },

            computed: {
                filteredGames: function() {
                    return this.games.filter(g => g.title.toLowerCase().includes(this.localFilter.search))
                },
            },

            mounted() {
                this.getPostsByFilter();
            },

            methods: {
                getPostsByFilter: function (filterData, category) {
                    const that = this;

                    if (filterData) {
                        this.perPage = this.step;
                        that.filterData = filterData;
                    }

                    if (category) {
                        that.category = category;
                    }

                    const data = {
                        perPage: that.perPage,
                        action: that.action,
                        ...(filterData || that.filterData),
                        category: category || that.category
                    };

                    $.ajax({
                        type: 'GET',
                        url: '/wp-admin/admin-ajax.php',
                        dataType: 'json',
                        data: data,
                        success: function(response){
                            that.loaded = true;
                            that.games = response;
                        }
                    });
                },

                loadMore(e) {
                    e.preventDefault();

                    this.showMore = false;
                    this.perPage = 99999;
                    this.getPostsByFilter();
                }
            }
        })




        headerScroll();

        const swiper = new Swiper('.banner__slider', {
            speed: 1200,
            loop: true,
            pagination: {
                el: '.banner__slider-pagination',
                clickable: true,
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: false,
            },
        });

        $('.button__menu').click(function() {
            $(this).toggleClass("button__menu_active");
            $('.navigation__container').toggleClass("navigation__container_visible");
            $('body').toggleClass("body_overflow");
            $('.header').toggleClass("header_active");
        })
    });

    $(window).on('scroll', () => {
        headerScroll();
    })

    function headerScroll() {
        const scrollTop = $(this).scrollTop();
        const header = $('.header')

        if (scrollTop > 50) {
            header.addClass('header_fixed');
        } else {
            header.removeClass('header_fixed');
        }
    }
}(jQuery));
