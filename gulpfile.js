const gulp = require("gulp");
const styles = require("./gulp/tasks/styles");
const scripts = require("./gulp/tasks/scripts");
const serve = require("./gulp/tasks/server");

const dev = gulp.parallel(styles, scripts);

const build = gulp.series(dev);

module.exports.start = gulp.series(build, serve);
module.exports.build = build;