<?php
/**
 * The template for displaying only on pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package playfortuna
 */
/* Template Name:  News*/
/* Template post type: page */
get_header();
?>

	<main id="primary" class="site-main">
        <div class="page page-news">
            <div class="news-wrap">
                <div class="title-box page-news__title-box">
                    <h1 class="title title-decor title-decor_red"><?php wp_title(); ?></h1>
                </div>
                <div id="news-list-app">
                    <div class="promo-list">
                        <?php
                        $getCatId = get_field("id_cat");
                        $query = new WP_Query('posts_per_page=50&orderby=date&cat='.$getCatId);
                        if ($query->have_posts()) { ?>
                            <?php while ($query->have_posts()) {
                                $query->the_post(); ?>
                                <div class="promo-list__item">
                                    <?php the_field('type_news'); ?>
                                    <div class="promo-item">
                                        <a href="<?php the_permalink() ?>" class="promo-item__link">
                                            <?php if (has_post_thumbnail()) { ?>
                                                <?php echo get_the_post_thumbnail( $page->ID, 'thumbnail', array('class' => 'promo-item__image'));  } else { ?>
                                                <img src="<?php echo first_post_image() ?>" class="promo-item__image" alt="<?php the_title(); ?>"/>
                                            <?php } ?>
                                            <span class="promo-item__title"><?php the_title(); ?></span>
                                        </a>
                                    </div>
                                </div>
                            <?php }
                            wp_reset_postdata();  } else echo ' '; ?>
                    </div>
                </div>
            </div>
        </div>
	</main>
<?php
get_footer();
