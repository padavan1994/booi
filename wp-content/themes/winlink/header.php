<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package winlink
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="content">
    <header class="header">
        <div class="container header__container">
            <div class="header__logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
            </div>
            <nav class="header__nav">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'primary-menu',
                    )
                );
                ?>
            </nav>
            <div class="header__controls">
                <?php
                if (have_rows('register_buttons', 'option')):
                    while (have_rows('register_buttons', 'option')): the_row(); ?>
                        <a href="<?php the_sub_field('register', 'option'); ?>" class="header__controls-item reg"></a>
                        <a href="<?php the_sub_field('login', 'option'); ?>" class="header__controls-item avt"></a>
                    <?php endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </header><!-- #masthead -->
