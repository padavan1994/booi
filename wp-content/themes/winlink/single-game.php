<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package jozz
 */
/* Template Name:  Games*/
/* Template post type: post */
get_header();
?>
    <script type="application/ld+json" class="aioseop-schema">
        {"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"<?php echo home_url(); ?>#organization","url":"<?php echo home_url(); ?>","name":"<?php echo get_bloginfo( 'description', 'display' ) ?>","sameAs":[]},

            {"@type":"WebSite","@id":"<?php echo home_url(); ?>#website","url":"<?php echo home_url(); ?>","name":"<?php echo get_bloginfo( 'description', 'display' ) ?>","publisher":{"@id":"<?php echo home_url(); ?>/#organization"}},

            {"@type":"WebPage","@id":"<?php global $wp; echo home_url( $wp->request ) ?>#webpage","url":"<?php global $wp; echo home_url( $wp->request ) ?>","inLanguage":"ru-RU","name":"<?php $post->post_title?>","isPartOf":{"@id":"<?php echo home_url(); ?>/#website"},"datePublished":"<?php echo $post->post_date ?>","dateModified":"<?php echo $post->post_modified ?>","description":"<?php echo $post->post_content ?>"},

            {"@type":"Article","@id":"<?php global $wp; echo home_url( $wp->request ) ?>#article","isPartOf":{"@id":"<?php global $wp; echo home_url( $wp->request ) ?>#webpage"},"author":{"@id":"<?php echo the_author_meta( 'user_url' , $post->post_author); ?>#author"},"headline":"<?php echo $post->post_content ?>","datePublished":"<?php echo $post->post_date ?>","dateModified":"<?php echo $post->post_modified ?>","commentCount":"<?php echo $post->comment_count ?>","mainEntityOfPage":{"@id":"<?php global $wp; echo home_url( $wp->request ) ?>#webpage"},"publisher":{"@id":"<?php echo home_url(); ?>/#organization"},"articleSection":""},
            {"@type":"Person","@id":"<?php echo the_author_meta( 'user_url' , $post->post_author); ?>#author","name":"<?php echo the_author_meta( 'user_nicename' , $post->post_author); ?>","sameAs":[],"image":{"@type":"ImageObject","@id":"<?php echo home_url(); ?>/#personlogo","url":"<?php echo the_author_meta( 'avatar' , $post->post_author); ?>","width":96,"height":96,"caption":"Iplayfortuna"}}]}
    </script>


    <section class="main__wrapper">
        <div class="game">
            <div class="container game__container">
                <div class="game__buttons">
                    <a href="<?php the_field('play_btn'); ?>" class="btn btn-shining">Играть на деньги</a>
                </div>
                <div class="content">
                    <h1 class="game__title">Игровой автомат Treasure Skyland - играть онлайн</h1>
                </div>
                <div class="game__wrapper">
                    <iframe src="<?php the_field('game_link'); ?>" class="iframe-demo-game"></iframe>
                </div>
                <div class="content">

                </div>
            </div>
        </div>

    <section>
<?php
get_footer();
