<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package winlink
 */

get_header();
?>
    <div class="page page-404">
    <div class="page-404-content">
        <div class="page-404__title">
            Oops, an error occurred
        </div>
        <div class="page-404__subtitle">
            404
        </div>
        <div class="page-404__info">
            Page is not found
        </div>
    </div>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-shining page-404__btn">
        На главную
    </a>
</div>
<?php
get_footer();
