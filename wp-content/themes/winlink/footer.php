<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package winlink
 */

?>
<footer class="footer">
    <div class="container footer__container">
        <div class="footer__links">
            <div class="footer__links-title">Важно знать</div>
            <?php
            wp_nav_menu(array(
                'theme_location' => 'menu-2',
                'menu_id' => 'Footer',
            ));
            ?>
        </div>
        <div class="footer__partners">
            <div class="footer__partners-top">
                <div class="footer__payments-list">
                    <div class="footer__payments-list-item">Visa</div>
                    <div class="footer__payments-list-item">Mastercard</div>
                    <div class="footer__payments-list-item">Qiwi</div>
                    <div class="footer__payments-list-item">Webmoney</div>
                    <div class="footer__payments-list-item">Yandex money</div>
                    <div class="footer__payments-list-item">Skrill</div>
                    <div class="footer__payments-list-item">Netller</div>
                </div>
            </div>
            <div class="footer__partners-bottom">
                <div class="footer__providers-list">
                    <ul>
                        <?php
                        if (have_rows('provider', 'option')):
                            while (have_rows('provider', 'option')): the_row(); ?>
                                <li>
                                    <a href="<?php the_sub_field('logo_link', 'option'); ?>">
                                        <?php $value_provider = get_sub_field('logo'); ?>
                                        <img src="<?php echo $value_provider['url']; ?>" alt="<?php echo $value_provider['alt']; ?>">
                                    </a>
                                </li>
                            <?php endwhile;
                        else :
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="footer__providers-list-bot">
                    <ul>
                        <li><img src="img/providers/redtiger.png" alt=""></li>
                        <li><img src="img/providers/redtiger.png" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container footer-information">
        <div class="site-information">
            <?php the_field('copyright', 'option'); ?>
        </div>
    </div>
</footer>
</div><!-- #page -->
<script id="storefront-json-ld-organization" type="application/ld+json">{
        "@context": "http://schema.org/",
        "@type": "Casino",
        "name": "<?php echo get_bloginfo('name'); ?>",
        "url": "<?php echo get_site_url(); ?>",
        "image": {
            "@type": "ImageObject",
            "url": "<?php echo get_custom_logo(); ?>",
            "height": "318",
            "width": "400"
        },
        "logo": {
            "@type": "ImageObject",
            "url": "<?php echo get_custom_logo(); ?>",
            "height": "318",
            "width": "400"
        },
        "description": "<?php echo get_bloginfo('description'); ?>",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "<?php the_field('copyright', 'option'); ?>"
        }
    }</script>
<?php wp_footer(); ?>

</body>
</html>
