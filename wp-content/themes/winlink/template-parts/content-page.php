<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package winlink
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class("article-item"); ?>>
    <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

    <?php echo get_the_post_thumbnail( $page->ID, 'thumbnail', array('class' => 'article-image')); ?>

    <div class="entry-content">
        <?php
        the_content();
        ?>
    </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
