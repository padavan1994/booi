<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jozz
 */

?>
<article class="article-item" itemscope itemtype="http://schema.org/Event">
    <?php
    if ( is_singular() ) :
        the_title( '<h1 class="title">', '</h1>' );
    else :
        the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
    endif;
    ?>
    <script id="storefront-json-ld-page-post" type="application/ld+json">{
            "@context": "http://schema.org/",
            "@type": "Article",
            "headline": "<?php the_title(); ?>",
            "alternativeHeadline": "<?php the_title(); ?>",
            "description": "<?php echo the_content(); ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo get_the_post_thumbnail_url(); ?>",
                "height": "1000",
                "width": "600"
            },
            "datePublished": "<?php echo get_the_date(); ?>",
            "dateModified": "<?php echo get_the_modified_date(); ?>",
            "author": {
                "@type": "Person",
                "name": "<?php echo get_the_author(); ?>"
            },
            "mainEntityOfPage": "<?php echo get_permalink(); ?>"
        }</script>
    <div class="article-date-box">
        <svg xmlns="http://www.w3.org/2000/svg" class="article-date-icon">
            <path d="M13.998,1H12v0.5c0,0.518-0.263,0.975-0.662,1.244c-0.24,0.162-0.528,0.256-0.839,0.256
                                         C9.671,3.001,9,2.329,9,1.5V1H6v0.5c0,0.518-0.263,0.975-0.662,1.244C5.098,2.906,4.81,3.001,4.498,3.001
                                         C3.671,3.001,3,2.329,3,1.5V1H0.998C0.447,1,0,1.447,0,2v14h15V2C15,1.447,14.551,1,13.998,1z M14,9.998c0,3-3,2-3,2
                                         S12.001,15,9.001,15H1V5h13V9.998z M10.5,2C10.776,2,11,1.776,11,1.5v-1C11,0.224,10.776,0,10.5,0C10.224,0,10,0.224,10,0.5v1
                                         C10,1.776,10.224,2,10.5,2z M4.5,2C4.776,2,5,1.776,5,1.5v-1C5,0.224,4.776,0,4.5,0C4.224,0,4,0.224,4,0.5v1
                                         C4,1.776,4.224,2,4.5,2z M4,9H2v2h2V9z M4,12H2v2h2V12z M7,12H5v2h2V12z M7,9H5v2h2V9z M10,9H8v2h2V9z M13,9h-2v2h2V9z M7,6H5v2h2
                                         V6z M10,6H8v2h2V6z M13,6h-2v2h2V6z"></path>
        </svg>
        <?php the_date(); ?>
    </div>

    <?php echo get_the_post_thumbnail( $page->ID, 'thumbnail', array('class' => 'article-image')); ?>

	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'jozz' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
