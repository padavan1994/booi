<?php
/**
 * The template for displaying only on pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package playfortuna
 */
/* Template Name:  Tournament*/
/* Template post type: page */
get_header();
?>
    <div class="page page-tournament">
        <div class="container page-tournament__container">
            <div class="tournament-wrap">
                <div class="title-box page-news__title-box">
                    <h1 class="title title-decor title-decor_red">Турниры
                    </h1>
                </div>

                <div class="page-tournament__block">
                    <div class="page-tournament__subtitle">Активные турниры</div>
                    <div class="page-tournament__items">
                        <?php
                        if (have_rows('active_tournaments')):
                            while (have_rows('active_tournaments')): the_row(); ?>
                                <div class="tournament-item tournament-item_bg">
                                    <div class="tournament-description">
                                        <div class="tournament-description__item-1">
                                            <div class="tournament-title">
                                                <a href="<?php the_sub_field('name_t_link'); ?>"><?php the_sub_field('name_t'); ?></a>
                                            </div>

                                            <div class="tournament-list-item-info">
                                                <div class="tournament-list-item-info__title">Дата проведения турнира</div>
                                                <div class="tournament-list-item-info__value"><?php the_sub_field('date_t'); ?></div>
                                            </div>
                                            <div class="tournament-list-item-info">
                                                <div class="tournament-list-item-info__title">Тип турнира</div>
                                                <div class="tournament-list-item-info__value"><?php the_sub_field('type_t'); ?></div>
                                            </div>
                                            <div class="tournament-list-item-info">
                                                <div class="tournament-list-item-info__title">Количество призовых мест</div>
                                                <div class="tournament-list-item-info__value"><?php the_sub_field('sum_t'); ?></div>
                                            </div>
                                        </div>
                                        <div class="tournament-point tournament-description__item-3">
                                            <div class="tournament-point-icon-box">
                                                <div class="tournament-point__count">
                                                    <?php the_sub_field('points_t'); ?>
                                                    <span>Поинты</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-box-center">
                                        <a href="<?php the_sub_field('take_part_t'); ?>" class="btn btn-shining">Участвовать
                                        </a>
                                    </div>
                                </div>
                            <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>
                <div class="page-tournament__block">
                    <div class="page-tournament__subtitle">Завершенные турниры</div>
                    <div class="page-tournament__items">
                        <?php
                        if (have_rows('inactive_tournaments')):
                            while (have_rows('inactive_tournaments')): the_row(); ?>
                                <div class="tournament-item tournament-item_bg">
                                    <div class="tournament-description">
                                        <div class="tournament-description__item-1">
                                            <div class="tournament-title">
                                                <a href="<?php the_sub_field('name_t_link'); ?>"><?php the_sub_field('name_t'); ?></a>
                                            </div>

                                            <div class="tournament-list-item-info">
                                                <div class="tournament-list-item-info__title">Дата проведения турнира</div>
                                                <div class="tournament-list-item-info__value"><?php the_sub_field('date_t'); ?></div>
                                            </div>
                                            <div class="tournament-list-item-info">
                                                <div class="tournament-list-item-info__title">Тип турнира</div>
                                                <div class="tournament-list-item-info__value"><?php the_sub_field('type_t'); ?></div>
                                            </div>
                                            <div class="tournament-list-item-info">
                                                <div class="tournament-list-item-info__title">Количество призовых мест</div>
                                                <div class="tournament-list-item-info__value"><?php the_sub_field('sum_t'); ?></div>
                                            </div>
                                        </div>
                                        <div class="tournament-point tournament-description__item-3">
                                            <div class="tournament-point-icon-box">
                                                <div class="tournament-point__count">
                                                    <?php the_sub_field('points_t'); ?>
                                                    <span>Поинты</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
