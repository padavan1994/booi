<?php
/**
 * The template for displaying only on pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package playfortuna
 */
/* Template Name:  Home*/
/* Template post type: page */
get_header();
?>
    <div class="banner">
        <div class="banner__container container">
            <div class="banner__slider-wrapper">
                <div class="swiper-container banner__slider">
                    <div class="swiper-wrapper">
                        <?php
                        if (have_rows('main_slider', 'option')):
                            while (have_rows('main_slider', 'option')): the_row(); ?>
                                <div class="swiper-slide banner__slider-item">
                                    <a href="<?php the_sub_field('main_slider-link', 'option'); ?>">
                                        <?php $value_slide = get_sub_field('main_slider-image', 'option'); ?>
                                        <img src="<?php echo $value_slide['url']; ?>"
                                             alt="<?php echo $value_slide['alt']; ?>">
                                    </a>
                                </div>
                            <?php endwhile;
                        else :
                        endif;
                        ?>
                    </div>
                </div>
                <div class="banner__slider-pagination-wrap">
                    <div class="banner__slider-pagination"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-menu">
        <div class="container page-menu__container">
            <div class="page-menu__inner">
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-3',
                        'menu_id' => 'Game_types',
                    )
                );
                ?>
            </div>
        </div>
    </div>

    <div class="games" id="games">
        <div class="container games__container">
            <?php
            $tags = get_tags();
            $mappedTags = array();

            foreach ($tags as $tag) {
                $tag_link = get_tag_link($tag->term_id);
                $tag->logo = get_field('provider_tag_logo', $tag);
                $tag->active = false;
                array_push($mappedTags, $tag);
            }
            ?>
            <custom-filter
                    name='Провайдеры'
                    default-value='Все провайдеры'
                    v-on:filter-change="getPostsByFilter($event)"
                    v-on:sort-change="filterLocal($event)"
                    v-on:volatility-change="filterLocal($event)"
                    :items='<?php echo json_encode($mappedTags) ?>'></custom-filter>
            <div class="games__list">
                <a :href="game.link" class="game" v-for="game in filteredGames" :key="game.id">
                    <template v-if="game.image">
                        <img :src="game.image" alt="">
                    </template>
                    <template v-else>
                        <img :src="game.fistImage ? game.fistImage : '/wp-content/themes/jozz/img/no_image.png'"
                             alt="">
                    </template>
                    <div class="game__info">
                        {{game.title}}
                    </div>
                </a>
                <div class="game _more" v-if="showMore" v-on:click="loadMore()">
                    +
                    <div class="game__info">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-block">
        <div class="container text-block__container">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
            endwhile; else: ?>
                <p></p>
            <?php endif; ?>
        </div>
    </div>

    <script type="text/x-template" id="filter-template">
        <div class="games__filters">
            <div class="games__filters-row">
                <div class="games__filters-col _left">
                    <div class="games__select">
                        <div class="games__select-button"
                             :class="{active: isProviderOpened}"
                             v-on:click="toggleProvidersMenu()"
                        >
                            <span>Провайдеры</span>
                        </div>
                        <div class="games__select-list">
                            <div class="games__select-list-inner">
                                <div class="games__select-list-item"
                                     v-on:click="chooseOption(0)"
                                     :class="{active: activeOption === 0}"
                                >Все провайдеры
                                </div>
                                <div class="games__select-list-item"
                                     v-for="item in items"
                                     :class="{active: activeOption === item.term_id}"
                                     v-on:click="chooseOption(item.term_id)"
                                >
                                    <img :src="item.logo" alt="item.name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="games__filters-col _right">
                    <div class="games__sort">
                        <div class="games__sort-title">Сотрировка:</div>
                        <div class="games__sort-list">
                            <div class="games__sort-list-item" v-on:click="setSorting('')" :class="{active: sortBy === ''}">Default</div>
                            <div class="games__sort-list-item" v-on:click="setSorting('min_bet')" :class="{active: sortBy === 'min_bet'}">Min bet</div>
                            <div class="games__sort-list-item" v-on:click="setSorting('max_bet')" :class="{active: sortBy === 'max_bet'}">Max bet</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="games__filters-row">
                <div class="games__filters-col _left">
                    <div class="games__select js-game-select">
                        <div class="games__select-button"
                             :class="{active: isVolatilityOpened}"
                             v-on:click="toggleVolatilityMenu()"
                        >
                            <span>Волатильность</span>
                        </div>
                        <div class="games__select-list">
                            <div class="games__select-list-inner">
                                <div class="games__select-list-item" v-on:click="selectVolatility('')" :class="{active: volatility === ''}">Все</div>
                                <div class="games__select-list-item" v-on:click="selectVolatility('Низкая')" :class="{active: volatility === 'Низкая'}">Низкая</div>
                                <div class="games__select-list-item" v-on:click="selectVolatility('Средняя')" :class="{active: volatility === 'Средняя'}">Средняя</div>
                                <div class="games__select-list-item" v-on:click="selectVolatility('Высокая')" :class="{active: volatility === 'Высокая'}">Высокая</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="games__filters-col _right"></div>
            </div>
        </div>
    </script>
<?php
get_footer();
