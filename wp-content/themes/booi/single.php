<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package booi
 */

get_header();
?>
    <script id="storefront-json-ld-page-post" type="application/ld+json">{
            "@context": "http://schema.org/",
            "@type": "Article",
            "headline": "<?php the_title(); ?>",
            "alternativeHeadline": "<?php the_title(); ?>",
            "description": "<?php echo the_content(); ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo get_the_post_thumbnail_url(); ?>",
                "height": "1000",
                "width": "600"
            },
            "datePublished": "<?php echo get_the_date(); ?>",
            "dateModified": "<?php echo get_the_modified_date(); ?>",
            "author": {
                "@type": "Person",
                "name": "<?php echo get_the_author(); ?>"
            },
            "mainEntityOfPage": "<?php echo get_permalink(); ?>"
        }</script>
	<main id="primary" class="site-main">
        <div class="container">
            <div class="content">
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );


		endwhile; // End of the loop.
		?>

            </div></div>
	</main><!-- #main -->

<?php
get_footer();
