<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package booi
 */

?>

<footer class="footer">
    <div class="container footer__container">
        <div class="footer__wrapper">
            <aside class="footer__aside">
                <nav class="footer-navigation">
                    <span class="footer-navigation__title">Важно знать:</span>
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-2',
                            'menu_id'        => 'footer-menu',
                        )
                    );
                    ?>
                </nav>
            </aside>
            <div class="footer__main">
                <div class="payments">
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="visa / mastercard" class="payments_icon payments_icon-visa "></span>
                        </div>
                        <span class="payments__title">visa / mastercard</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="maestro" class="payments_icon payments_icon-maestro "
                                 ></span>
                        </div>
                        <span class="payments__title">maestro</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="qiwi" class="payments_icon payments_icon-qiwi "></span>
                        </div>
                        <span class="payments__title">qiwi</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="webmoney" class="payments_icon payments_icon-webmoney "
                                 ></span>
                        </div>
                        <span class="payments__title">webmoney</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="yandex money" class="payments_icon payments_icon-yandex "
                                 ></span>
                        </div>
                        <span class="payments__title">yandex money</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="skrill" class="payments_icon payments_icon-skrill "
                                 ></span>
                        </div>
                        <span class="payments__title">skrill</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="neteller" class="payments_icon payments_icon-neteller "
                                 ></span>
                        </div>
                        <span class="payments__title">neteller</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="Interac e-Transfer"
                                  class="payments_icon payments_icon-interac-etransfer "></span>
                        </div>
                        <span class="payments__title">Interac e-Transfer</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="Interac Online" class="payments_icon payments_icon-interac-online "
                                 ></span>
                        </div>
                        <span class="payments__title">Interac Online</span>
                    </div>
                    <div class="payments__item">
                        <div class="payments__item-wrapper">
                            <span title="Express-Connect" class="payments_icon payments_icon-express-connect "
                                 ></span>
                        </div>
                        <span class="payments__title">Express-Connect</span>
                    </div>
                </div>
                <div class="partners">
                    <div class="partners__item">
                        <span title="1x2Gaming" class="partners_icon partners_icon-1x2Gaming "></span>
                    </div>
                    <div class="partners__item">
                        <span title="2by2Gaming" class="partners_icon partners_icon-2by2Gaming "></span>
                    </div>
                    <div class="partners__item">
                        <span title="ainsworth" class="partners_icon partners_icon-ainsworth "></span>
                    </div>
                    <div class="partners__item">
                        <span title="amatic" class="partners_icon partners_icon-amatic "></span>
                    </div>
                    <div class="partners__item">
                        <span title="belatra" class="partners_icon partners_icon-belatra "></span>
                    </div>
                    <div class="partners__item">
                        <span title="betsoft" class="partners_icon partners_icon-betsoft "></span>
                    </div>
                    <div class="partners__item">
                        <span title="bfgames" class="partners_icon partners_icon-bfgames "></span>
                    </div>
                    <div class="partners__item">
                        <span title="bigtimegaming" class="partners_icon partners_icon-bigtimegaming "></span>
                    </div>
                    <div class="partners__item">
                        <span title="blueprint" class="partners_icon partners_icon-blueprint "></span>
                    </div>
                    <div class="partners__item">
                        <span title="booming-games" class="partners_icon partners_icon-booming-games "></span>
                    </div>
                    <div class="partners__item">
                        <span title="booongo" class="partners_icon partners_icon-booongo "></span>
                    </div>
                    <div class="partners__item">
                        <span title="elk" class="partners_icon partners_icon-elk "></span>
                    </div>
                    <div class="partners__item">
                        <span title="endorphina" class="partners_icon partners_icon-endorphina "></span>
                    </div>
                    <div class="partners__item">
                        <span title="evolutiongaming" class="partners_icon partners_icon-evolutiongaming "></span>
                    </div>
                    <div class="partners__item">
                        <span title="ganapati" class="partners_icon partners_icon-ganapati "></span>
                    </div>
                    <div class="partners__item">
                        <span title="genesis" class="partners_icon partners_icon-genesis "></span>
                    </div>
                    <div class="partners__item">
                        <span title="habanero" class="partners_icon partners_icon-habanero "></span>
                    </div>
                    <div class="partners__item">
                        <span title="irondog" class="partners_icon partners_icon-irondog "></span>
                    </div>
                    <div class="partners__item">
                        <span title="justforwin" class="partners_icon partners_icon-justforwin "></span>
                    </div>
                    <div class="partners__item">
                        <span title="netent" class="partners_icon partners_icon-netent "></span>
                    </div>
                    <div class="partners__item">
                        <span title="nolimitcity" class="partners_icon partners_icon-nolimitcity "></span>
                    </div>
                    <div class="partners__item">
                        <span title="playngo" class="partners_icon partners_icon-playngo "></span>
                    </div>
                    <div class="partners__item">
                        <span title="playson" class="partners_icon partners_icon-playson "></span>
                    </div>
                    <div class="partners__item">
                        <span title="pragmatic" class="partners_icon partners_icon-pragmatic "></span>
                    </div>
                    <div class="partners__item">
                        <span title="pushgaming" class="partners_icon partners_icon-pushgaming "></span>
                    </div>
                    <div class="partners__item">
                        <span title="quickfire" class="partners_icon partners_icon-quickfire "></span>
                    </div>
                    <div class="partners__item">
                        <span title="quickspin" class="partners_icon partners_icon-quickspin "></span>
                    </div>
                    <div class="partners__item">
                        <span title="rabcat" class="partners_icon partners_icon-rabcat "></span>
                    </div>
                    <div class="partners__item">
                        <span title="redtiger" class="partners_icon partners_icon-redtiger "></span>
                    </div>
                    <div class="partners__item">
                        <span title="relax" class="partners_icon partners_icon-relax "></span>
                    </div>
                    <div class="partners__item">
                        <span title="spinomenal" class="partners_icon partners_icon-spinomenal "></span>
                    </div>
                    <div class="partners__item">
                        <span title="thunderkick" class="partners_icon partners_icon-thunderkick "></span>
                    </div>
                    <div class="partners__item">
                        <span title="tomhorn" class="partners_icon partners_icon-tomhorn "></span>
                    </div>
                    <div class="partners__item">
                        <span title="wazdan" class="partners_icon partners_icon-wazdan "></span>
                    </div>
                    <div class="partners__item">
                        <span title="yggdrasil" class="partners_icon partners_icon-yggdrasil "></span>
                    </div>
                </div>
            </div>
        </div>
        <span class="copyright">
            <?php the_field('copyright', 'option'); ?>
            BOOI is operated by NETGLOBE SERVICES LTD acting as a payment processor, a company with a registered address at 79 Spyrou Kyprianou Avenue, Protopapas Building, 2nd floor, office 201, 3076 Limassol, Cyprus. <br>Registration number HE 313864 <br> GLOBONET B.V., a company with registered address at Kaya Richard J. Beaujon Z / N, Curaçao, is licensed by the Government of Curacao under the No.1668 / JAZ. <br>© Netglobe Services Limited, 2019 - 2021. All rights reserved.
        </span>
    </div>
</footer>
<script id="storefront-json-ld-organization" type="application/ld+json">{
        "@context": "http://schema.org/",
        "@type": "Casino",
        "name": "<?php echo get_bloginfo('name'); ?>",
        "url": "<?php echo get_site_url(); ?>",
        "image": {
            "@type": "ImageObject",
            "url": "<?php echo get_custom_logo(); ?>",
            "height": "318",
            "width": "400"
        },
        "logo": {
            "@type": "ImageObject",
            "url": "<?php echo get_custom_logo(); ?>",
            "height": "318",
            "width": "400"
        },
        "description": "<?php echo get_bloginfo('description'); ?>",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "<?php the_field('copyright', 'option'); ?>"
        }
    }</script>
<?php wp_footer(); ?>

</body>
</html>
