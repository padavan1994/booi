<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package booi
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
    <header class="header">
        <div class="container header__container">
            <nav class="navigation">
                <a href="/" class="logo" aria-label="Booi Home">

                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="">
                </a>
                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_id'        => 'header-menu',
                        'menu_class'   => 'navigation__list',
                        'container_class'   => 'navigation__container',
                        'add_li_class' => 'navigation__item',
                        'link_class' => 'navigation__link',
                    )
                );
                ?>
            </nav>
            <div class="navigation__links">
                <?php
                if (have_rows('register_buttons', 'option')):
                    while (have_rows('register_buttons', 'option')): the_row(); ?>
                        <a href="<?php the_sub_field('register', 'option'); ?>" class="button_yellow button_first button_md button_anim">Регистрация</a>
                        <a href="<?php the_sub_field('login', 'option'); ?>" class="button_pure button_last button_md button_anim">Войти</a>
                    <?php endwhile;
                else :
                endif;
                ?>
            </div>
            <div class="button__menu t_hidden">
                <span class="button__menu-line"></span>
            </div>
        </div>
    </header>
