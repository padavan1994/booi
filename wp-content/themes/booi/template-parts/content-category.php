<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package jozz
 */

?>
<li class="catalog__item" class="game">
    <div class="catalog__item_wrapper">
        <picture class="catalog__pct">
            <?php if (has_post_thumbnail()) {
                echo get_the_post_thumbnail( $page->ID, 'thumbnail' );  } else { ?>
                <img src="<?php echo first_post_image() ?>" class="img_absolute catalog__img"
                     alt="<?php the_title(); ?>" >
            <?php } ?>
        </picture>
        <div class="catalog__hover">
            <a href="<?php the_permalink() ?>" class="catalog__title"><?php the_title(); ?></a>
            <a href="<?php the_permalink() ?>" class="catalog__circle">
            </a>
        </div>
    </div>
</li>
