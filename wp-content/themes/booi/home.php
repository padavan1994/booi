<?php
/**
 * The template for displaying only on pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package playfortuna
 */
/* Template Name:  Home*/
/* Template post type: page */
get_header();
?>
<div class="banner">
    <div class="banner__container">
        <div class="banner__slider-wrapper">
            <div class="swiper-container banner__slider">
                <div class="swiper-wrapper">
                    <?php
                    if (have_rows('slider_mainPage')):
                        while (have_rows('slider_mainPage')): the_row(); ?>
                            <div class="swiper-slide banner__slider-item">
                                <a href="<?php the_sub_field('slider_mainPage-lnk'); ?>">
                                    <?php $value_slide = get_sub_field('slider_mainPage-img'); ?>
                                    <img src="<?php echo $value_slide['url']; ?>"
                                         alt="<?php echo $value_slide['alt']; ?>">
                                </a>
                            </div>
                        <?php endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
            <div class="banner__slider-pagination-wrap">
                <div class="banner__slider-pagination"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="content content__title">
        <h1 class="main__title"><?php the_field('title__h1'); ?></h1>
    </div>
    <div class="categories">
        <?php if( have_rows('cats_menu') ): ?>

            <ul class="categories__list">
                <?php while( have_rows('cats_menu') ): the_row();
                    // переменные
                    $image = get_sub_field('cats_menu-img');
                    $content = get_sub_field('cats_menu-title');
                    $link = get_sub_field('cats_menu-lnk');

                    ?>
                    <li class="categories__item">
                        <?php if( $link ): ?>
                        <a href="<?php echo $link; ?>" class="categories__link">
                            <?php endif; ?>
                            <?php echo $image; ?>
                            <?php echo $content; ?>
                            <?php if( $link ): ?>
                        </a>
                    <?php endif; ?>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>

        <ul class="categories__list">
            <?php
            if (have_rows('cats_menu', 'option')):
                while (have_rows('cats_menu', 'option')): the_row(); ?>
                    <li class="categories__item">
                        <a href="<?php the_sub_field('cats_menu-lnk', 'option'); ?>" class="categories__link">
                            <?php $value_catsImg = get_sub_field('cats_menu-img'); ?>
                            <img src="<?php echo $value_catsImg['url']; ?>" alt="<?php echo $value_catsImg['alt']; ?>">
                            <?php the_sub_field('cats_menu-title', 'option'); ?>
                        </a>
                    </li>
                <?php endwhile;
            else :
            endif;
            ?>
        </ul>
    </div>


    <div class="catalog" id="games">
        <form aria-label="Search form" class="form" role="search">
            <div class="form__box input">
                <label class="lable__box">
                    <span class="hidden">Поиск</span>
                    <input v-model="localFilter.search" type="text" aria-label="Search input" class="search__catalog_line" placeholder="Поиск">
                </label>
                <button class="search__catalog_btn" aria-label="search button">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17" class="search__catalog_svg"><path d="M9.819 5.43A4.374 4.374 0 005.44 1.05a4.379 4.379 0 104.379 4.38zM5.44 10.874a5.445 5.445 0 110-10.891 5.445 5.445 0 010 10.89zm3.063-1.618l.754-.754 1.472 1.472-.754.754-1.472-1.472zm.754-.754l1.472 1.472-.754.754-1.472-1.472.754-.754z"></path><path d="M14.284 14.996l.72-.72-3.911-3.91-.72.72 3.911 3.91zm-3.191-6.14l5.42 5.419-2.228 2.23-5.42-5.42 2.228-2.228z"></path></svg>
                </button>
            </div>
        </form>
        <template v-if="!filteredGames.length && loaded"><ul class="catalog__list catalog__list_search active"><span class="catalog__none">Игра не найдена</span></ul></template>
        <ul class="catalog__list">
            <li class="catalog__item" class="game" v-for="game in filteredGames" :key="game.id">
                <div class="catalog__item_wrapper">
                    <picture class="catalog__pct">
                        <template v-if="game.image">
                            <img :src="game.image" alt="" class="img_absolute catalog__img">
                        </template>
                        <template v-else>
                            <img :src="game.fistImage ? game.fistImage : '/wp-content/themes/booi/img/no_image.png'" class="img_absolute catalog__img"
                                 alt="">
                        </template>
                    </picture>
                    <div class="catalog__hover">
                        <a :href="game.link" class="catalog__title">{{game.title}}</a>
                        <a :href="game.link" class="catalog__circle">
                        </a>
                    </div>
                </div>
            </li>
        </ul>
        <template v-if="showMore" >
            <div class="catalog__all">
                <div class="catalog__all_bg "></div>
                <a href="#" v-on:click="loadMore($event)" class="button-line button-line_pure">Посмотреть все</a>
            </div>
        </template>
    </div>


    <div class="content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
            the_content();
        endwhile; else: ?>
            <p></p>
        <?php endif; ?>

        <a href="<?php the_field('btnMain__login'); ?>" class="button_pure">Пройти регистрацию</a>
    </div>
</div>
<?php get_footer(); ?>
