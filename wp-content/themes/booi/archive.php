<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package booi
 */

get_header();
?>

    <script type="application/ld+json" class="aioseop-schema">
        {"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"<?php echo home_url(); ?>#organization","url":"<?php echo home_url(); ?>","name":"<?php echo get_bloginfo( 'description', 'display' ) ?>","sameAs":[]},

            {"@type":"WebSite","@id":"<?php echo home_url(); ?>#website","url":"<?php echo home_url(); ?>","name":"<?php echo get_bloginfo( 'description', 'display' ) ?>","publisher":{"@id":"<?php echo home_url(); ?>/#organization"}},

            {"@type":"WebPage","@id":"<?php global $wp; echo home_url( $wp->request ) ?>#webpage","url":"<?php global $wp; echo home_url( $wp->request ) ?>","inLanguage":"ru-RU","name":"<?php $category->name ?>","isPartOf":{"@id":"<?php echo home_url(); ?>/#website"},"datePublished":"","dateModified":"","description":"<?php $category->description ?>"},

            {"@type":"Article","@id":"<?php global $wp; echo home_url( $wp->request ) ?>#article","isPartOf":{"@id":"<?php global $wp; echo home_url( $wp->request ) ?>#webpage"},"author":{"@id":"#author"},"headline":"<?php $category->description ?>","datePublished":"","dateModified":"","commentCount":"","mainEntityOfPage":{"@id":"<?php global $wp; echo home_url( $wp->request ) ?>#webpage"},"publisher":{"@id":"<?php global $wp; echo home_url( $wp->request ) ?>#organization"},"articleSection":""},

            {"@type":"Person","@id":"#author","name":"Iplayfortuna","sameAs":[],"image":{"@type":"ImageObject","@id":"#personlogo","url":"","width":96,"height":96,"caption":"Iplayfortuna"}}]}
    </script>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

            <div class="container">
                <div class="content content__title">
                    <header class="page-header">
                        <h1 class="main__title"><?php
                        $term = get_queried_object();
                        $title = get_field('title__h1', $term);
                            echo $title; ?></h1>
                    </header>
                </div>


                <div class="catalog" id="games">
                    <ul class="catalog__list">
                        <?php
                        /* Start the Loop */
                        while ( have_posts() ) :
                            the_post();
                            get_template_part( 'template-parts/content-category', get_post_type() );
                        endwhile; ?>
                    </ul>
                </div>


                <div class="content">
                    <?php the_archive_description( '<div class="archive-description">', '</div>' ); ?>
                </div>
            </div>

		<?php else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();
