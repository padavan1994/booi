<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package winlink
 */

get_header();
?>
    <script id="storefront-json-ld-website" type="application/ld+json">{
            "@context": "http://schema.org/",
            "@type": "WebSite",
            "url": "<?php echo get_site_url(); ?>",
            "name": "<?php echo get_bloginfo( 'name' ); ?>",
            "alternateName": "<?php echo bloginfo('name'); ?>"
        }</script>
    <main id="primary" class="site-main">
        <div class="page news-wrap">

            <div class="container">
                <div class="content">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.
			?>

                </div>
            </div>
        </div>
    </main><!-- #main -->
<?php
get_footer();
