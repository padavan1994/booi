<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'booi' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qH%tee?3u@w!:x[yszQE6EbT(S(M_SJRQ$~=UdBLai[%(xQ}zui8+K_c@};S^A1H' );
define( 'SECURE_AUTH_KEY',  '2I29PnPNt+G1?X|^+{T.D< {T1O.i#f{B3yp.}bH&W:v5/p@vO+1_7%Zl:R-}C5V' );
define( 'LOGGED_IN_KEY',    'n$3l(#JSzu_=t3`pqR>RR3?tV})f2NQ2XHdmah]XXQizn{A*rF2esCm/_yTBDu8~' );
define( 'NONCE_KEY',        '<`&~ X-`M@9oUSmL..p+D*C`k){b>lskpbABC9j3t2ct(H;s63{a^X#[_kJY{cF[' );
define( 'AUTH_SALT',        'RnJ=Plk~KAMnU?D~EEq1[Efgt:34zC)7?TNPs<%Tpm+p^<lQjBZyl6TmO#:bwnN^' );
define( 'SECURE_AUTH_SALT', 'NgGw>kMEZ@2j*DZY=X-HyD`(-I87[il<,!;;7pR{iG4Mb.7teqXkp{QuT3Q*N(1;' );
define( 'LOGGED_IN_SALT',   'c.cYK,Qli&o;U)*U!qtqMIa9B[)-_BM0>],4WHNl7Aq!$zR]8d+m3@CyO73-eVG4' );
define( 'NONCE_SALT',       'ys.ZG|TEN)f+`qNx#/?ZS?#xFB4ZXN4Aup$6LP&D0jmg[1XQ@Ej1M:V=bBG[S>Ah' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
